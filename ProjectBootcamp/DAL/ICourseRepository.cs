﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public interface ICourseRepository : IDisposable
    {
        Task<IPagedList<UserCourse>> GetUserCourses(int? page, IQueryable<UserCourse> userCourses);
        Task<List<Course>> GetCourses();
        IQueryable<UserCourse> LoadUserCourses();
        IQueryable<UserCourse> GetUserCourseByUserId(string userId);
        Task<UserCourse> GetUserCourseById(int id);
        Task<Course> GetCourseById(int? id);
        Task<List<ApplicationUser>> GetUsersNotInACourse(int? id);
        Task<List<StudentFiles>> GetStudentFiles(string userId);
        Task<int> InsertUserCourse(int courseId, string userId);
        Task<int> InsertCourse(Course course, int courseId, string userId);
        Task<int> UpdateCourse(Course course);
        Task<int> DeleteUserCourse(UserCourse userCourse);
        Task<int> DeleteCourse(Course course);
        Task<int> InsertStudentFiles(StudentFiles studentFiles);
        Task<List<UserCourse>> GetUsersCourses(ApplicationUser user);
        Task<List<StudentFiles>> GetUsersFiles(ApplicationUser user);
        void RemoveRange(IEnumerable<UserCourse> courses, IEnumerable<StudentFiles> userFiles);
    }
}