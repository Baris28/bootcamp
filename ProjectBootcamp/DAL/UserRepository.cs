﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<ApplicationUser>> GetUsers(int? page)
        {
            return await (await _context.Users.ToListAsync()).ToPagedListAsync(page ?? 1, 5);
        }

        public ApplicationUser GetUserById(string id)
        {
            return _context.Users.Find(id);
        }

        public async Task<List<IdentityRole>> GetRoles()
        {
            return await _context.Roles.ToListAsync();
        }

        public void Remove(ApplicationUser user)
        {
            _context.Users.Remove(user);
        }

        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}