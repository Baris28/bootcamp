﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public class VideoRepository : IVideoRepository
    {
        private readonly ApplicationDbContext _context;

        public VideoRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Video>> LoadVideo()
        {
            return await _context.Videos.Include(v => v.Course).Include(v => v.VideoComments).ToListAsync();
        }

        public async Task<Video> GetVideoById(int? id)
        {
            return await _context.Videos.FindAsync(id);
        }

        public async Task<int> InsertVideoComments(Video video, string userId, VideoComment videoComment)
        {
            _context.VideoComments.Add(new VideoComment
            {
                VideoId = video.Id,
                UserId = userId,
                Comment = videoComment.Comment,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            });
            return await _context.SaveChangesAsync();
        }

        public async Task<int> InsertVideo(Video video)
        {
            _context.Videos.Add(video);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateVideo(Video video)
        {
            _context.Entry(video).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteVideo(Video video)
        {
            _context.Videos.Remove(video);
            return await _context.SaveChangesAsync();
        }

        public async Task<VideoComment> GetVideoCommentById(int id)
        {
            return await _context.VideoComments.FindAsync(id);
        }

        public async Task<int> DeleteVideoComment(VideoComment videoComment)
        {
            _context.VideoComments.Remove(videoComment);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<VideoComment>> GetUsersVideoComments(ApplicationUser user)
        {
            return await (await _context.VideoComments.ToListAsync()).Where(x => x.UserId == user.Id).ToListAsync();
        }

        public void RemoveRange(IEnumerable<VideoComment> videoComments)
        {
            _context.VideoComments.RemoveRange(videoComments);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}