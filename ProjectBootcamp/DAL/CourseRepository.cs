﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public class CourseRepository : ICourseRepository
    {
        private readonly ApplicationDbContext _context;

        public CourseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<UserCourse>> GetUserCourses(int? page, IQueryable<UserCourse> userCourses)
        {
            return await userCourses.ToListAsync().Result.GroupBy(u => u.CourseId).Select(u => u.FirstOrDefault())
                .OrderBy(u =>
                {
                    if (u != null) return u.CourseId;
                    throw new InvalidOperationException();
                }).ToPagedListAsync(page ?? 1, 8);
        }

        public async Task<List<Course>> GetCourses()
        {
            return await _context.Courses.ToListAsync();
        }

        public IQueryable<UserCourse> LoadUserCourses()
        {
            return _context.UserCourses.Include(u => u.Course).Include(u => u.User);
        }

        public IQueryable<UserCourse> GetUserCourseByUserId(string userId)
        {
            return _context.UserCourses.Where(x => x.UserId == userId);
        }

        public async Task<UserCourse> GetUserCourseById(int id)
        {
            return await _context.UserCourses.FindAsync(id);
        }

        public async Task<Course> GetCourseById(int? id)
        {
            return await _context.Courses.FindAsync(id);
        }

        public async Task<List<ApplicationUser>> GetUsersNotInACourse(int? id)
        {
            return await (await _context.Users.ToListAsync()).Where(x =>
                x.UserCourses.All(c => c.CourseId != id)).ToListAsync();
        }

        public async Task<List<StudentFiles>> GetStudentFiles(string userId)
        {
            return await (await _context.StudentFiles.ToListAsync()).Where(b => b.UserId == userId).ToListAsync();
        }

        public async Task<int> InsertUserCourse(int courseId, string userId)
        {
            _context.UserCourses.Add(new UserCourse {Id = Convert.ToInt32(null), CourseId = courseId, UserId = userId});
            return await _context.SaveChangesAsync();
        }

        public async Task<int> InsertCourse(Course course, int courseId, string userId)
        {
            _context.Courses.Add(course);
            _context.UserCourses.Add(new UserCourse
                {CourseId = courseId, UserId = userId}); // attaches the course that was made to the teacher/admin
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateCourse(Course course)
        {
            _context.Entry(course).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteUserCourse(UserCourse userCourse)
        {
            _context.UserCourses.Remove(userCourse ?? throw new InvalidOperationException());
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteCourse(Course course)
        {
            _context.Courses.Remove(course ?? throw new InvalidOperationException());
            return await _context.SaveChangesAsync();
        }

        public async Task<int> InsertStudentFiles(StudentFiles studentFiles)
        {
            _context.StudentFiles.Add(studentFiles);
            return await _context.SaveChangesAsync();
        }

        public async Task<List<UserCourse>> GetUsersCourses(ApplicationUser user)
        {
            return await (await _context.UserCourses.ToListAsync()).Where(x => x.UserId == user.Id).ToListAsync();
        }

        public async Task<List<StudentFiles>> GetUsersFiles(ApplicationUser user)
        {
            return await (await _context.StudentFiles.ToListAsync()).Where(x => x.UserId == user.Id).ToListAsync();
        }

        public void RemoveRange(IEnumerable<UserCourse> courses, IEnumerable<StudentFiles> userFiles)
        {
            _context.UserCourses.RemoveRange(courses);
            _context.StudentFiles.RemoveRange(userFiles);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}