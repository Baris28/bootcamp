﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public interface IForumRepository : IDisposable
    {
        Task<IPagedList<Forum>> GetForums(int? page);
        Task<Forum> GetForumById(int? id);
        Task<ForumComment> GetForumCommentById(int? id);
        Task<CommentHasUpVotes> GetForumCommentHasUpvotes(int? id);
        Task InsertForumComment(int? id, ForumComment forumComment, string userId);
        Task<int> DeleteForumComment(int? id);
        Task<int> UpvoteComment(int id, string userId);
        Task<int> DownvoteComment(CommentHasUpVotes commentHasUpVotes);
        Task<ForumComment> CurrentForumComment(ForumComment forumComment);
        Task<Forum> CurrentForum(Forum forum);
        Task<int> InsertForum(string userId, Forum forum);
        Task<int> UpdateForum(Forum currentForum, string title, string description, DateTime now);
        Task DeleteForum(Forum forum);
        Task<int> SaveChangesAsync();
        Task<List<Forum>> GetUsersForumPosts(ApplicationUser user);
        Task<List<ForumComment>> GetUsersForumComments(ApplicationUser user);
        void RemoveRange(List<Forum> forums, List<ForumComment> forumComment);
    }
}