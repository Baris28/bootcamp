﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectBootcamp.Models;

namespace ProjectBootcamp.DAL
{
    public interface IVideoRepository : IDisposable
    {
        Task<List<Video>> LoadVideo();
        Task<Video> GetVideoById(int? id);
        Task<int> InsertVideoComments(Video video, string userId, VideoComment videoComment);
        Task<int> InsertVideo(Video video);
        Task<int> UpdateVideo(Video video);
        Task<int> DeleteVideo(Video video);
        Task<VideoComment> GetVideoCommentById(int id);
        Task<int> DeleteVideoComment(VideoComment videoComment);
        Task<List<VideoComment>> GetUsersVideoComments(ApplicationUser user);
        void RemoveRange(IEnumerable<VideoComment> videoComments);
    }
}