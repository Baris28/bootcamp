﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public class ForumRepository : IForumRepository
    {
        private readonly ApplicationDbContext _context;

        public ForumRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IPagedList<Forum>> GetForums(int? page)
        {
            return await _context.Forums.ToListAsync().Result.GroupBy(u => u.Id).Select(x => x.FirstOrDefault())
                .OrderBy(u => u?.Id).ToPagedListAsync(page ?? 1, 8);
        }

        public async Task<Forum> GetForumById(int? id)
        {
            return await _context.Forums.FindAsync(id);
        }

        public async Task<ForumComment> GetForumCommentById(int? id)
        {
            return await _context.ForumComments.FindAsync(id);
        }

        public async Task<CommentHasUpVotes> GetForumCommentHasUpvotes(int? id)
        {
            return await _context.CommentHasUpVotes.FindAsync(id);
        }

        public async Task InsertForumComment(int? id, ForumComment forumComment, string userId)
        {
            var forum = await _context.Forums.FindAsync(id);
            if (forum != null)
                _context.ForumComments.Add(new ForumComment
                    {ForumId = forum.Id, UserId = userId, Comment = forumComment.Comment});
            await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteForumComment(int? id)
        {
            var forumComment = await _context.ForumComments.FindAsync(id);

            _context.ForumComments.Remove(forumComment ?? throw new InvalidOperationException());
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpvoteComment(int id, string userId)
        {
            _context.CommentHasUpVotes.Add(new CommentHasUpVotes {CommentId = id, UserId = userId});
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DownvoteComment(CommentHasUpVotes commentHasUpVotes)
        {
            _context.CommentHasUpVotes.Remove(commentHasUpVotes);
            return await _context.SaveChangesAsync();
        }

        public async Task<ForumComment> CurrentForumComment(ForumComment forumComment)
        {
            return await _context.ForumComments.FirstOrDefaultAsync(p => p.Id == forumComment.Id);
        }

        public async Task<Forum> CurrentForum(Forum forum)
        {
            return await _context.Forums.FirstOrDefaultAsync(p => p.Id == forum.Id);
        }

        public async Task<int> InsertForum(string userId, Forum forum)
        {
            forum.UserId = userId;
            _context.Forums.Add(forum);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateForum(Forum currentForum, string title, string description, DateTime now)
        {
            currentForum.Title = title;
            currentForum.Description = description;
            currentForum.UpdatedAt = now;
            return await _context.SaveChangesAsync();
        }

        public async Task DeleteForum(Forum forum)
        {
            _context.Forums.Remove(forum ?? throw new InvalidOperationException());
            await _context.SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public async Task<List<Forum>> GetUsersForumPosts(ApplicationUser user)
        {
            return await (await _context.Forums.ToListAsync()).Where(x => x.UserId == user.Id).ToListAsync();
        }

        public async Task<List<ForumComment>> GetUsersForumComments(ApplicationUser user)
        {
            return await (await _context.ForumComments.ToListAsync()).Where(x => x.UserId == user.Id).ToListAsync();
        }

        public void RemoveRange(List<Forum> forums, List<ForumComment> forumComment)
        {
            _context.Forums.RemoveRange(forums);
            _context.ForumComments.RemoveRange(forumComment);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}