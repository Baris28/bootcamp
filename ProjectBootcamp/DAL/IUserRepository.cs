﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjectBootcamp.Models;
using X.PagedList;

namespace ProjectBootcamp.DAL
{
    public interface IUserRepository : IDisposable
    {
        Task<IPagedList<ApplicationUser>> GetUsers(int? page);
        ApplicationUser GetUserById(string id);
        Task<List<IdentityRole>> GetRoles();
        void Remove(ApplicationUser user);
        Task<int> SaveChangesAsync();
    }
}