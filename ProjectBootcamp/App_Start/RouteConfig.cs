﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectBootcamp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapMvcAttributeRoutes();
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                "Videos",
                "{courseController}/{courseAction}/{courseId}/{controller}/{action}/{id}",
                new
                {
                    courseController = "courses",
                    courseAction = "Details",
                    courseId = UrlParameter.Optional,
                    controller = "Videos",
                    action = "Details",
                    id = UrlParameter.Optional
                }
            );
        }
    }
}