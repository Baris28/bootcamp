﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectBootcamp.Models
{
    [Table("Video")]
    public class Video
    {
        [Key] public int Id { get; set; }

        [DisplayName("Titel")] [Required] public string Title { get; set; }

        [DisplayName("Descriptie")] [Required] public string Description { get; set; }

        public int Views { get; set; }
        public string Url { get; set; }
        public byte Activated { get; set; }

        [ForeignKey("Course")] public int CourseId { get; set; }

        public virtual Course Course { get; set; }
        public virtual ICollection<VideoComment> VideoComments { get; set; }
        public virtual ICollection<StudentFiles> StudentFiles { get; set; }

        [DisplayName("Aangemaakt op")] public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DisplayName("Aangepast op")] public DateTime UpdatedAt { get; set; } = DateTime.Now;

        public string TeacherFileName { get; set; }
    }

    public class VideoComment
    {
        [Key] public int Id { get; set; }

        [Required] public string Comment { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [DisplayName("Video")] public int VideoId { get; set; }

        [DisplayName("Gebruiker")] public string UserId { get; set; }

        [ForeignKey("VideoId")]
        [Column(Order = 1)]
        public Video Video { get; set; }

        [ForeignKey("UserId")]
        [Column(Order = 2)]
        public virtual ApplicationUser User { get; set; }
    }

    public class StudentFiles
    {
        [Key] public int Id { get; set; }

        [Required] public string FileName { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public bool Submitted { get; set; } = false;

        [DisplayName("Video")] public int VideoId { get; set; }

        [DisplayName("Gebruiker")] public string UserId { get; set; }

        [ForeignKey("VideoId")]
        [Column(Order = 1)]
        public virtual Video Video { get; set; }

        [ForeignKey("UserId")]
        [Column(Order = 2)]
        public virtual ApplicationUser User { get; set; }
    }
}