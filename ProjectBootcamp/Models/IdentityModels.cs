﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ProjectBootcamp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual ICollection<UserCourse> UserCourses { get; set; }
        public virtual ICollection<VideoComment> VideoComments { get; set; }
        public virtual ICollection<CommentHasUpVotes> CommentHasUpVotes { get; set; }

        public virtual ICollection<Forum> Forums { get; set; }
        public virtual ICollection<ForumComment> ForumComments { get; set; }
        public virtual ICollection<StudentFiles> StudentFiles { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", false)
        {
        }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Video> Videos { get; set; }

        public DbSet<UserCourse> UserCourses { get; set; }
        public override IDbSet<ApplicationUser> Users { get; set; }

        public DbSet<Forum> Forums { get; set; }

        public DbSet<VideoComment> VideoComments { get; set; }

        public DbSet<ForumComment> ForumComments { get; set; }
        public DbSet<CommentHasUpVotes> CommentHasUpVotes { get; set; }

        public DbSet<StudentFiles> StudentFiles { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}