﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectBootcamp.Models
{
    [Table("Course")]
    public class Course
    {
        [Key] public int Id { get; set; }

        [DisplayName("Titel")] [Required] public string Title { get; set; }

        [DisplayName("Descriptie")] [Required] public string Description { get; set; }

        public virtual ICollection<UserCourse> UserCourses { get; set; }
        public virtual ICollection<Video> Videos { get; set; }

        [DisplayName("Aangemaakt op")] public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DisplayName("Aangepast op")] public DateTime UpdatedAt { get; set; } = DateTime.Now;

        [ForeignKey("Color")]
        [DisplayName("Color")]
        [Required]
        public int ColorId { get; set; }

        public virtual CourseColor Color { get; set; }
    }

    public class UserCourse
    {
        [Key] public int Id { get; set; }

        [DisplayName("Cursus")] public int CourseId { get; set; }

        [DisplayName("Gebruiker")] public string UserId { get; set; }

        [ForeignKey("CourseId")]
        [Column(Order = 1)]
        public virtual Course Course { get; set; }

        [ForeignKey("UserId")]
        [Column(Order = 2)]
        public virtual ApplicationUser User { get; set; }
    }

    public class CourseColor
    {
        [Key] public int Id { get; set; }

        [DisplayName("Color")] public string Color { get; set; }
    }
}