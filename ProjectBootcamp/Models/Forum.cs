﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectBootcamp.Models
{
    [Table("Forum")]
    public class Forum
    {
        [Key] public int Id { get; set; }

        [Required] [DisplayName("Titel")] public string Title { get; set; }

        [Required] [DisplayName("Descriptie")] public string Description { get; set; }

        [ForeignKey("ApplicationUser")] public string UserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<ForumComment> ForumComments { get; set; }

        [DisplayName("Aangemaakt op")] public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DisplayName("Gewijzigd op")] public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }

    public class ForumComment
    {
        [Key] public int Id { get; set; }

        [Required] [DisplayName("Commentaar")] public string Comment { get; set; }

        public int Likes { get; set; } = 0;
        public int ForumId { get; set; }
        public string UserId { get; set; }

        [DisplayName("Aangemaakt op")] public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DisplayName("Gewijzigd op")] public DateTime UpdatedAt { get; set; } = DateTime.Now;

        [ForeignKey("ForumId")]
        [Column(Order = 1)]
        public virtual Forum Forum { get; set; }

        [ForeignKey("UserId")]
        [Column(Order = 2)]
        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<CommentHasUpVotes> CommentHasUpVotes { get; set; }
    }

    public class CommentHasUpVotes
    {
        [Key] public int Id { get; set; }

        public int CommentId { get; set; }
        public string UserId { get; set; }

        [ForeignKey("CommentId")]
        [Column(Order = 1)]
        public virtual ForumComment ForumComment { get; set; }
    }
}