﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NLog;
using ProjectBootcamp.DAL;
using ProjectBootcamp.Models;

namespace ProjectBootcamp.Controllers
{
    public class VideosController : Controller
    {
        private readonly ILogger _logger;
        private readonly IVideoRepository _videoRepository;

        public VideosController(IVideoRepository videoRepository, ILogger logger)
        {
            _videoRepository = videoRepository;
            _logger = logger;
        }

        // GET: Videos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.Videos = await _videoRepository.LoadVideo();
            var video = await _videoRepository.GetVideoById(id);
            if (video == null) return HttpNotFound();

            if (Directory.Exists(HttpContext.ApplicationInstance.Server.MapPath("~") + "Uploads/courses/course_" +
                                 video.CourseId + "/videoid_" + id))
            {
                var filePaths = Directory.GetFiles(HttpContext.ApplicationInstance.Server.MapPath("~") +
                                                   "Uploads/courses/course_" + video.CourseId + "/videoid_" + id);
                ViewBag.FilePaths = filePaths;
            }
            else
            {
                ViewBag.FilePaths = null;
            }

            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(video);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Details(int? id,
            [Bind(Include = "Id,Comment,UserId,VideoId,CreatedAt,UpdatedAt")]
            VideoComment videoComment)
        {
            try
            {
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                var video = await _videoRepository.GetVideoById(id);
                if (video != null)
                    await _videoRepository.InsertVideoComments(video, User.Identity.GetUserId(), videoComment);
                return RedirectToAction("Details");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Videos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Title,Description,Views,Activated,CourseId,CreatedAt,UpdatedAt,Url")]
            Video video)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _videoRepository.InsertVideo(video);
                    return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
                }

                return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // GET: Videos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var video = await _videoRepository.GetVideoById(id);
            if (video == null) return HttpNotFound();
            return View(video);
        }

        // POST: Videos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "Id,Title,Description,Views,Activated,CourseId,CreatedAt,UpdatedAt,Url")]
            Video video)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _videoRepository.UpdateVideo(video);
                    return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
                }

                return View(video);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Videos/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var video = await _videoRepository.GetVideoById(id);
                if (video != null) await _videoRepository.DeleteVideo(video);
                return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Videos/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteComment([Bind(Include = "Id,CreatedAt,UpdatedAt,Comment,VideoId,UserId")]
            int id)
        {
            try
            {
                var video = await _videoRepository.GetVideoCommentById(id);
                if (video != null) await _videoRepository.DeleteVideoComment(video);
                return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _videoRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}