﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NLog;
using ProjectBootcamp.DAL;
using ProjectBootcamp.Models;

namespace ProjectBootcamp.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ICourseRepository _courseRepository;
        private readonly ILogger _logger;

        public CoursesController(ICourseRepository courseRepository, ILogger logger)
        {
            _courseRepository = courseRepository;
            _logger = logger;
        }

        // GET: Courses
        [Authorize]
        public async Task<ActionResult> Index(int? page)
        {
            var userCourses = _courseRepository.LoadUserCourses();
            // show ur own courses
            if (!User.IsInRole("Admin"))
            {
                var loggedInStudentId = User.Identity.GetUserId();
                userCourses = _courseRepository.GetUserCourseByUserId(loggedInStudentId);
            }

            return View(await _courseRepository.GetUserCourses(page, userCourses));
        }

        // GET: Courses/Details/5
        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var userCourse = await _courseRepository.GetCourseById(id);
            ViewBag.UserId = new SelectList(await _courseRepository.GetUsersNotInACourse(id), "Id", "Email");
            if (userCourse == null) return HttpNotFound();

            ViewBag.Id = id;
            ViewBag.Student = await _courseRepository.GetStudentFiles(User.Identity.GetUserId());

            return View(userCourse);
        }

        // POST: Courses/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<ActionResult> Create([Bind(Include = "Id,CourseId,UserId")] UserCourse userCourse)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _courseRepository.InsertUserCourse(userCourse.Id, userCourse.UserId);
                    return Redirect(Request.UrlReferrer?.ToString());
                }

                return Redirect(Request.UrlReferrer?.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Courses/CreateCourse
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<ActionResult> CreateCourse(
            [Bind(Include = "Id,Title,Description,CreatedAt,UpdatedAt,ColorId")]
            Course course)
        {
            try
            {
                if (!ModelState.IsValid) return Redirect(Request.UrlReferrer?.ToString());
                await _courseRepository.InsertCourse(course, course.Id, User.Identity.GetUserId());
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Courses/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Description,CreatedAt,UpdatedAt,ColorId")]
            Course course)
        {
            try
            {
                if (!ModelState.IsValid) return Redirect(Request.UrlReferrer?.ToString());
                await _courseRepository.UpdateCourse(course);
                return Redirect(Request.UrlReferrer?.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Courses/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var userCourse = await _courseRepository.GetUserCourseById(id);
                if (User.Identity.GetUserId() == userCourse?.UserId) return RedirectToAction("Index");
                await _courseRepository.DeleteUserCourse(userCourse);
                return Redirect(Request.UrlReferrer?.ToString()); // redirects to previous page
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Courses/DeleteCourse/5
        [HttpPost]
        [ActionName("DeleteCourse")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin, Teacher")]
        public async Task<ActionResult> DeleteCourseConfirmed(int id)
        {
            try
            {
                var course = await _courseRepository.GetCourseById(id);
                await _courseRepository.DeleteCourse(course);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        //Uploads A file for a Teacher
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file, int? id, int? videoId)
        {
            try
            {
                if (file.FileName == string.Empty) return Redirect(Request.UrlReferrer?.ToString());
                var folder = Server.MapPath("~/Uploads/courses/course_" + id + "/videoid_" + videoId);
                if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Uploads/courses/course_" + id + "/videoid_" + videoId),
                    fileName ?? throw new InvalidOperationException());
                file.SaveAs(path);

                return Redirect(Request.UrlReferrer?.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        //Uploads a file for a student
        [HttpPost]
        public async Task<ActionResult> UploadFileStudent(HttpPostedFileBase file, string student, int? courseid,
            int videoId, string userid)
        {
            try
            {
                if (file.FileName == string.Empty) return Redirect(Request.UrlReferrer?.ToString());
                var folder = Server.MapPath("~/Uploads/students/" + student + "/courses/" + "/courseid_" + courseid +
                                            "/videos/" + "/videoid_" + videoId);
                if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(
                    Server.MapPath("~/Uploads/students/" + student + "/courses/" + "/courseid_" + courseid +
                                   "/videos/" + "/videoid_" + videoId),
                    fileName ?? throw new InvalidOperationException());
                var objfiles = new StudentFiles
                {
                    FileName = fileName,
                    VideoId = videoId,
                    UserId = userid,
                    Submitted = true
                };
                ViewBag.FileName = fileName;
                await _courseRepository.InsertStudentFiles(objfiles);
                file.SaveAs(path);

                return Redirect(Request.UrlReferrer?.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _courseRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}