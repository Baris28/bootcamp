﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using NLog;
using ProjectBootcamp.DAL;
using ProjectBootcamp.Models;

namespace ProjectBootcamp.Controllers
{
    public class ForumsController : Controller
    {
        private readonly IForumRepository _forumRepository;
        private readonly ILogger _logger;

        public ForumsController(IForumRepository forumRepository, ILogger logger)
        {
            _forumRepository = forumRepository;
            _logger = logger;
        }

        // GET: Forums
        public async Task<ActionResult> Index(int? page)
        {
            ViewBag.Forums = await _forumRepository.GetForums(page);
            return View(ViewBag.Forums);
        }

        // GET: Forums/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            try
            {
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                var forum = await _forumRepository.GetForumById(id);

                if (forum == null) return HttpNotFound();
                return View(await _forumRepository.GetForumById(id));
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Forums/details
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Details(int? id,
            [Bind(Include = "Id,Comment,UserId,ForumId,CreatedAt,UpdatedAt")]
            ForumComment forumComment)
        {
            try
            {
                var forum = await _forumRepository.GetForumById(id);
                if (!ModelState.IsValid)
                    if (forum != null)
                        return RedirectToAction("Details", new {id = (int?) forum.Id});
                if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                await _forumRepository.InsertForumComment(id, forumComment, User.Identity.GetUserId());

                return RedirectToAction("Details", new {id = forum?.Id});
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        [Authorize]
        [HttpPost]
        [ActionName("DeleteComment")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteComment(int id)
        {
            try
            {
                var forumComment = await _forumRepository.GetForumCommentById(id);

                if (forumComment?.UserId == User.Identity.GetUserId() ||
                    forumComment?.Forum.UserId == User.Identity.GetUserId() || User.IsInRole("Admin") ||
                    User.IsInRole("Teacher")) await _forumRepository.DeleteForumComment(id);

                if (forumComment != null) return RedirectToAction("Details", new {id = forumComment.ForumId});
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

            _logger.Error(typeof(InvalidOperationException));
            throw new InvalidOperationException();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LikeComment(int id)
        {
            try
            {
                var forumComment = await _forumRepository.GetForumCommentById(id);
                if (!ModelState.IsValid)
                    if (forumComment != null)
                        return RedirectToAction("Details", new {id = forumComment.ForumId});

                await _forumRepository.UpvoteComment(id, User.Identity.GetUserId());

                if (forumComment != null) return RedirectToAction("Details", new {id = forumComment.ForumId});
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

            _logger.Error(typeof(InvalidOperationException));
            throw new InvalidOperationException();
        }

        [Authorize]
        [HttpPost]
        [ActionName("DeleteLikeFromComment")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteLikeFromComment(int id)
        {
            try
            {
                var commentHasUpVotes = await _forumRepository.GetForumCommentHasUpvotes(id);
                if (commentHasUpVotes == null) throw new InvalidOperationException();
                var forumComment = await _forumRepository.GetForumCommentById(commentHasUpVotes.CommentId);


                if (commentHasUpVotes.UserId == User.Identity.GetUserId())
                    await _forumRepository.DownvoteComment(commentHasUpVotes);

                if (forumComment != null) return RedirectToAction("Details", new {id = forumComment.ForumId});
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }

            _logger.Error(typeof(InvalidOperationException));
            throw new InvalidOperationException();
        }


        /// Edit forum comments
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditComment([Bind(Include = "Id,Comment")] ForumComment forumComment)
        {
            try
            {
                var currentComment = await _forumRepository.CurrentForumComment(forumComment);
                if (currentComment == null) return HttpNotFound();
                if (!ModelState.IsValid) return RedirectToAction("Details", new {id = currentComment.ForumId});
                if (currentComment.UserId != User.Identity.GetUserId())
                    return RedirectToAction("Details", new {id = currentComment.ForumId});
                currentComment.Comment = forumComment.Comment;
                await _forumRepository.SaveChangesAsync();

                return RedirectToAction("Details", new {id = currentComment.ForumId});
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Forums/Create
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Description,UserId,CreatedAt,UpdatedAt")]
            Forum forum)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _forumRepository.InsertForum(User.Identity.GetUserId(), forum);
                    return RedirectToAction("Index");
                }

                return View(forum);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Forums/Edit/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Description")]
            Forum forum)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var currentForum = await _forumRepository.CurrentForum(forum);
                    if (currentForum == null)
                        return HttpNotFound();

                    if (currentForum.ApplicationUser.Id != User.Identity.GetUserId() && !User.IsInRole("Admin") &&
                        !User.IsInRole("Teacher")) return RedirectToAction("Details", new {currentForum.Id});
                    await _forumRepository.UpdateForum(currentForum, forum.Title, forum.Description, DateTime.Now);

                    return RedirectToAction("Details", new {currentForum.Id});
                }

                return View(forum);
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        // POST: Forums/Delete/5
        [Authorize]
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var forum = await _forumRepository.GetForumById(id);
                if (forum?.ApplicationUser.Id != User.Identity.GetUserId() && !User.IsInRole("Admin") &&
                    !User.IsInRole("Teacher")) return RedirectToAction("Index");
                await _forumRepository.DeleteForum(forum);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _forumRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}