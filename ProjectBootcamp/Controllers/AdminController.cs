﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using NLog;
using ProjectBootcamp.DAL;

namespace ProjectBootcamp.Controllers
{
    public class AdminController : Controller
    {
        private readonly ICourseRepository _courseRepository;
        private readonly IForumRepository _forumRepository;
        private readonly ILogger _logger;
        private readonly IUserRepository _userRepository;
        private readonly IVideoRepository _videoRepository;

        // Constructor
        public AdminController(IUserRepository userRepository, IForumRepository forumRepository,
            ICourseRepository courseRepository, IVideoRepository videoRepository, ILogger logger)
        {
            _userRepository = userRepository;
            _forumRepository = forumRepository;
            _courseRepository = courseRepository;
            _videoRepository = videoRepository;
            _logger = logger;
        }

        // GET: Admin
        [Authorize]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index(int? page)
        {
            return View(await _userRepository.GetUsers(page));
        }

        // GET: Admin/Edit/5
        [Authorize]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var users = _userRepository.GetUserById(id);
            ViewBag.Roles = new SelectList(await _userRepository.GetRoles(), "Name", "Name");
            if (users == null) return HttpNotFound();
            return View(users);
        }

        // GET: Admin/Delete/5
        [Authorize]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var users = _userRepository.GetUserById(id);
            if (users == null) return HttpNotFound();
            return View(users);
        }

        // POST: Admin/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            try
            {
                var users = _userRepository.GetUserById(id);
                var courses = await _courseRepository.GetUsersCourses(users);
                var forums = await _forumRepository.GetUsersForumPosts(users);
                var forumComments = await _forumRepository.GetUsersForumComments(users);
                var videoComments = await _videoRepository.GetUsersVideoComments(users);
                var userFiles = await _courseRepository.GetUsersFiles(users);

                _userRepository.Remove(users);
                _courseRepository.RemoveRange(courses, userFiles);
                _forumRepository.RemoveRange(forums, forumComments);
                _videoRepository.RemoveRange(videoComments);

                await _userRepository.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) _userRepository.Dispose();
            base.Dispose(disposing);
        }
    }
}