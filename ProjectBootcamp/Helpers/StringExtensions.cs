﻿namespace ProjectBootcamp.Helpers
{
    public static class StringExtensions
    {
        public static string SubStringTo(this string thatString, int limit)
        {
            if (string.IsNullOrEmpty(thatString)) return string.Empty;
            return thatString.Length > limit ? thatString.Substring(0, limit) + "..." : thatString;
        }
    }
}