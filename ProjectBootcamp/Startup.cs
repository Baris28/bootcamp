﻿using Microsoft.Owin;
using Owin;
using ProjectBootcamp;

[assembly: OwinStartupAttribute(typeof(Startup))]

namespace ProjectBootcamp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}