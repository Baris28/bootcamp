using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Extras.NLog;
using Autofac.Integration.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjectBootcamp.DAL;
using ProjectBootcamp.Models;

namespace ProjectBootcamp
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var builder = new ContainerBuilder();
            builder.RegisterModule<NLogModule>();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterSource(new ViewRegistrationSource());

            builder.RegisterFilterProvider();

            // Register repositories
            builder.RegisterType<ForumRepository>().As(typeof(IForumRepository));
            builder.RegisterType<UserRepository>().As(typeof(IUserRepository));
            builder.RegisterType<CourseRepository>().As(typeof(ICourseRepository));
            builder.RegisterType<VideoRepository>().As(typeof(IVideoRepository));

            // Register db context
            builder.RegisterType<ApplicationDbContext>().AsSelf()
                .As<IdentityDbContext<ApplicationUser>>().InstancePerLifetimeScope();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}