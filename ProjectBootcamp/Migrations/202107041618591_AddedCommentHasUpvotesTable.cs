﻿using System.Data.Entity.Migrations;

namespace ProjectBootcamp.Migrations
{
    public partial class AddedCommentHasUpvotesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.CommentHasUpVotes",
                    c => new
                    {
                        Id = c.Int(false, true),
                        CommentId = c.Int(false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ForumComments", t => t.CommentId, true)
                .Index(t => t.CommentId);
        }

        public override void Down()
        {
            DropForeignKey("dbo.CommentHasUpVotes", "CommentId", "dbo.ForumComments");
            DropIndex("dbo.CommentHasUpVotes", new[] {"CommentId"});
            DropTable("dbo.CommentHasUpVotes");
        }
    }
}