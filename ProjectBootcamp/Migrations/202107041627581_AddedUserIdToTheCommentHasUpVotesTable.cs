﻿using System.Data.Entity.Migrations;

namespace ProjectBootcamp.Migrations
{
    public partial class AddedUserIdToTheCommentHasUpVotesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommentHasUpVotes", "UserId", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.CommentHasUpVotes", "UserId");
        }
    }
}